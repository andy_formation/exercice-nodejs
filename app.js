var path = require('path');
var express = require("express");
var app = express();

var sqlite3 = require('sqlite3').verbose();
const db_name = "etudiantsDB";
let db = new sqlite3.Database(db_name, (err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Connected to the SQlite database.');
  });
app.set('views', './views');
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static('public'));


const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

let title = "Voici mon application de gestion des etudiants"

//route pour l'accueil je renvoie la page d'accueil 
//avec un titre pour essayer de passer un paramètre
app.get("/", function(req, res) {
    res.render('index', {title: title});
})


//route pour aller au formulaire d'ajout
//j'ajoute un paramètre vide car j'en aurais besoin lors de la validation du formulaire
app.get("/ajout", function(req, res) {
    res.render('ajout');
})




const etudiantsPerPage = 5;

// Je fais ma requête sql dans la const sql et je l'execute via db all
// Mais pour chaque rows (=etudiant) j'appelle la fonction calculAge en lui passant en paramètre la date de naissance
// de l'etudiant et je rajoute a chaque rows une colonne "Age" qui prend la valeur de ce que retourne la fonction
// je renvoie ensuite la vue liste avec en paramètre le tableau des etudiants
app.get("/liste/:currentPage", (req, res) => {
    let currentPage = req.params.currentPage || 1;
    let first = (currentPage * etudiantsPerPage) - etudiantsPerPage;
    let nbEtudiantsSql = 'SELECT COUNT(*) as nb_etudiants FROM etudiants';
    db.get(nbEtudiantsSql, (err, result) => {
      if (err) {
      console.error(err.message);
      }
      let nbEtudiants = result.nb_etudiants;
      let page = Math.ceil(nbEtudiants / etudiantsPerPage);
   
    const sql = "SELECT * FROM etudiants LIMIT ?, ?";
    db.all(sql, [first, etudiantsPerPage], (err, rows) => {
      if (err) {
        return console.error(err.message);
      }
      rows = rows.map(row => {
        row.age = calculAge(row.dateNaissance);
        return row;
      });
      res.render('liste', { etudiants: rows, currentPage, page});   
    });
  });
});


//fonction qui calcul l'age et qui le retourne
//en paramètre on envoie la date de Naissance de l'etudiant qui est enregistrée dans la BDD
function calculAge(date) {
  var date = Array.from(date);
   date = date[5] + date[6] + "/" + date [8] + date [9]+ "/"+ date[0]+ date[1]+ date[2]
        + date[3]
  date = new Date(date);
  var month_diff = Date.now() -date.getTime();
  var age_dt = new Date(month_diff);
  var year = age_dt.getUTCFullYear();
  var age =  Math.abs(year - 1970);
  return(age);
}


app.post("/ajoutEtudiant", (req, res) => {
  const sql = "INSERT INTO etudiants (nom, prenom, dateNaissance, numCI) VALUES (?,?,?,?)";
  const params = [req.body.name, req.body.prenom, req.body.date, req.body.num];
  db.run(sql, params, (err) => {
    if (err) {
      return console.error(err.message);
    }
    res.redirect('/liste/5');   
  });
  
});

app.get("/formModif/:id", (req, res) => {
  const sql = "SELECT * FROM etudiants WHERE idEtudiants= ?";
  const params = [req.params.id];
    db.get(sql, params, (err, etudiant) => {
      if (err) {
        return console.error(err.message);
      }
      res.render('modif', { etudiantModif: etudiant});  
    });
});

app.post("/modifEtudiant/:id", (req, res) => {
  const sql = "UPDATE etudiants SET idEtudiants = ?, nom = ?, prenom = ?, dateNaissance = ?, numCI = ? WHERE idEtudiants = ?";
  const params = [req.body.id, req.body.name, req.body.prenom, req.body.date, req.body.num, req.body.id];
  db.run(sql, params, (err) => {
    if (err) {
      return console.error(err.message);
    }
    res.redirect('/liste/1');   
  });
});

app.get("/suppEtudiant/:id", (req, res) => {
  const sql = "DELETE from etudiants WHERE idEtudiants = ?";
  //pas req.body car je vais récupérer les données dans l'URL
  const params = [req.params.id];
  db.run(sql, params, (err) => {
    if (err) {
      return console.error(err.message);
    }
  }); 
  res.redirect('/liste/1');  
});


app.post("/listeBySearch", (req, res) => {
  const sql = "SELECT * FROM etudiants WHERE nom LIKE ? OR prenom LIKE ?";
  const params = [req.body.research+'%', req.body.research+'%'];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    rows = rows.map(row => {
      row.age = calculAge(row.dateNaissance);
      return row;
    });
    res.render('liste', { etudiants: rows, currentPage: "", page: ""});   
  });
});



/*app.post("/liste/:currentPage/:search", (req, res) => {
  let currentPage = req.params.currentPage || 1;
  let first = (currentPage * etudiantsPerPage) - etudiantsPerPage;
  let nbEtudiantsSql = 'SELECT COUNT(*) as nb_etudiants FROM etudiants';
  db.get(nbEtudiantsSql, (err, result) => {
    if (err) {
    console.error(err.message);
    }
    let nbEtudiants = result.nb_etudiants;
    let page = Math.ceil(nbEtudiants / etudiantsPerPage);
 
  const sql = "SELECT * FROM etudiants LIMIT ?, ?";
  if (req.query.search) {
    sql += "WHERE nom LIKE ? OR prenom LIKE ?"
  
  const params = [req.body.research+'%', req.body.research+'%'];
  db.all(sql, [params], [first, etudiantsPerPage], (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    rows = rows.map(row => {
      row.age = calculAge(row.dateNaissance);
      return row;
    });
 
    res.render('liste', { etudiants: rows, currentPage, page});   
  
  });
 } else {
  const sql = "SELECT * FROM etudiants LIMIT ?, ?";
  db.all(sql, [first, etudiantsPerPage], (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    rows = rows.map(row => {
      row.age = calculAge(row.dateNaissance);
      return row;
    });
    res.render('liste', { etudiants: rows, currentPage, page});   
  });
 }
});
});*/


app.listen(1337);
console.log('Server running at http://127.0.0.1:1337');