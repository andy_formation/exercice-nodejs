CREATE TABLE etudiants 
(
    idEtud INTEGER Primary Key NOT NULL AUTO_INCREMENT,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    dateNaissance DATE,
    numCI INTEGER
)